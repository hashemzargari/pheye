from django.apps import AppConfig


class TrytestConfig(AppConfig):
    name = 'tryTest'
