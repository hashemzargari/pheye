from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required


@login_required
def dashboard(request):
    from django.urls import reverse
    return HttpResponse(
        f'you are logged-in as {request.user.username} <br><a href = "{reverse("account_logout")}">LogOut</a>')
